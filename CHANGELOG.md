# Changelog

## 0.1.0

  - Initial version, includes a `futureMiddleware` that intercepts Futures and FutureActions, captures their results, and dispatches their results as Redux actions.
  - Included classes
    - `FutureAction`
    - `FutureFulfilledAction`
    - `FutureRejectedAction`
